import java.io.*;
import java.net.*;
import java.util.HashMap;

import javax.swing.plaf.synth.SynthSeparatorUI;

import java.util.*;

public class RegistrationServer {
	private static final int port = 5000;
	private static int nodePort = 1111;
	private static LinkedHashMap<Integer,String> onlineNodes = new LinkedHashMap<>();	
	private ServerSocket serSocket;
	private Socket socket;
	private ObjectOutputStream out_obj;
    private ObjectInputStream in_obj;
    private DataOutputStream out;
    private List<Integer> connectedNode;
	
	private void startService() throws Exception{
		System.out.println("The registration server starts...");
		serSocket = new ServerSocket(port);
		boolean running = true;
		while(running){
			socket = serSocket.accept();
			String ip = socket.getRemoteSocketAddress().toString().replace("/","").split(":")[0]; //get the client ip address.
			
			in_obj = new ObjectInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
			System.out.println("The node at IP " + ip + " is connecting");
			connectedNode = (ArrayList<Integer>)in_obj.readObject();
			int status = connectedNode.get(1);  // 0 or 1 (0: join the network; 1: leave the network)
			int nodeId = connectedNode.get(0);
			if(status == 0){			
				if(onlineNodes.containsKey(nodeId)){					
					out.writeInt(0); //tell the node this nodeId has already joined the network	
					out.flush();
				}else{
					out.writeInt(1); //tell the node this nodeId will joint the network				
					onlineNodes.put(nodeId, ip);
					System.out.println(socket);
					System.out.println("onlineNOde size = " + onlineNodes.size());
					notifyAllNodes();
				}			
			}
			else if(status == 1){
				onlineNodes.remove(nodeId);
				notifyAllNodes();
			}	
			for(int id : onlineNodes.keySet()){
				System.out.println(id + "  " + onlineNodes.get(id));
			}
		}
		
		
	}
	private void notifyAllNodes() throws IOException {
		for(int nodeId : onlineNodes.keySet()){
			System.out.println("nodeID = " + nodeId);
			String ip = onlineNodes.get(nodeId);
			Socket sender = new Socket(ip,nodePort + nodeId);
			out_obj =  new ObjectOutputStream(sender.getOutputStream());
			out_obj.flush();
			Message message = new Message();
			message.setKind(0);
			message.setOnlineNodes(onlineNodes);
			out_obj.writeObject(message);
			out_obj.flush();
			System.out.println(sender);
			sender.close();
		}
		
	}
	public static void main(String[] args) throws Exception{
		new RegistrationServer().startService();
	}

}
