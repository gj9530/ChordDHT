import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.*;

public class ChordNode {
	private BufferedReader typeIn;
	private String line;
	private String serverIp;
	private final int port = 5000;
	private final int receivingPort = 1111; //the port receiving online node information from the server
	private static HashMap<Integer,String> onlineNodes = new LinkedHashMap<>();
	private Socket socket;
	private ServerSocket receivingSocket; //the socket receives online node information from the server
	private ObjectOutputStream out_obj;
	private DataInputStream in;
	private int nodeId;
	private List<Integer>nodeRequest;
	private int power = 4;
	private int numOfNodes = (int) Math.pow(2,power);
	private void updateOnlineNodes(Message message){
		onlineNodes = message.getOnlineNodes();
	}

    private boolean isInt(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private void process() throws Exception{
    	boolean running = true;
    	typeIn = new BufferedReader(new InputStreamReader(System.in));
    	System.out.println("Enter the registration server IP address");
		serverIp = typeIn.readLine();
		System.out.println("Enter your node ID:");
		String num = typeIn.readLine();
		isNodeId(num);
		nodeId = Integer.valueOf(num);
		nodeRequest = new ArrayList<>();
		nodeRequest.add(nodeId);
		nodeRequest.add(0);
		try {
			socket = new Socket(serverIp,port);
			
		} catch (UnknownHostException e) {
			System.out.println("The host at this IP address is not online");
			System.exit(0);
		} 
		SocketThread sThread = new SocketThread();
		Thread t = new Thread(sThread);
		t.start();
		out_obj = new ObjectOutputStream(socket.getOutputStream());
		out_obj.flush();
		out_obj.writeObject(nodeRequest);
		out_obj.flush();
		in = new DataInputStream(socket.getInputStream());
		int serReply = in.readInt();
		if(serReply == 0){        //The node ID is being used.
			System.out.println("This node ID is being used!");
			socket.close();
			System.exit(0);
		}
		System.out.println("The node has joined the network. \n");	
		socket.close();
		while(running){
			System.out.println("Enter your request:");
			System.out.println("1. Print the fingertable");
			System.out.println("2. Submit a key");
			System.out.println("3. Lookup a key");
			System.out.println("4. Exit");
			processInput();		
		}

    	
    }
	private void isNodeId(String num) {
		boolean isInt= isInt(num);		
		if(!isInt){
			System.out.println("The input node ID is not an integer");
			System.exit(0);
		}
		int theID = Integer.valueOf(num);
		if(theID < 0 || theID > 15){
			System.out.println("The node ID is out of range.");
			System.exit(0);
		}
		
	}

	private void processInput() throws IOException {
		typeIn = new BufferedReader(new InputStreamReader(System.in));
		String input= typeIn.readLine();
		boolean isInt= isInt(input);	
		if(!isInt){
			System.out.println("Wrong command! Please enter 1 - 4");
			return;
		}
		int request = Integer.valueOf(input);
		if(request == 1){
			printFingertable();
		}else if (request == 2){
			submitKey();
		}else if (request == 3){
			lookupKey();
		}else if(request == 4){
			nodeRequest = new ArrayList<>();
			nodeRequest.add(nodeId);
			nodeRequest.add(1);
			socket = new Socket(serverIp,port);
			out_obj = new ObjectOutputStream(socket.getOutputStream());
			out_obj.flush();
			out_obj.writeObject(nodeRequest);
			out_obj.flush();
			socket.close();
			receivingSocket.close();
			System.exit(0);
			
			
		}
		
	}

	private void lookupKey() {
		// TODO Auto-generated method stub
		
	}

	private void submitKey() throws IOException {
		System.out.println();
		System.out.println("Enter your key:");
		typeIn = new BufferedReader(new InputStreamReader(System.in));
		String key = typeIn.readLine();
		System.out.println("Enter the node ID where the key goes to:");
		typeIn = new BufferedReader(new InputStreamReader(System.in));
		String num= typeIn.readLine();
		isNodeId(num);
		int toNodeId = Integer.valueOf(num);
		Message message = new Message();
		message.setKind(1)
		
		
		
		
	}

	private void printFingertable() {
		System.out.println(String.format("%-10s %-10s %-10s %-10s" , "i","k+2^i","successor","IP" ));
		for(int i = 0; i < power; i++){
			int k = (nodeId + (int) Math.pow(2,i)) % numOfNodes;
			int successor = getSuccessor(i);
			String ip = onlineNodes.get(successor);
			System.out.println(String.format("%-10d %-10d %-10d %-15s" , i,k,successor,ip ));
		}
		System.out.println();
		
	}

	private int getSuccessor(int i) {
		int successor = nodeId + (int) Math.pow(2,i);
		for(int j = 0; j < numOfNodes; j++){
			if(successor > (numOfNodes - 1)){
				successor = successor % numOfNodes;
			}	
			if(onlineNodes.containsKey(successor)){
				return successor;
			}
			successor++;

		}
		return successor;
	}

	public static void main(String[] args) throws Exception{
		new ChordNode().process();
	
		
		
		
	}
	


	class SocketThread implements Runnable{
		private Socket sk;
		@Override
		public void run() {			
			try {
				receivingSocket = new ServerSocket(receivingPort + nodeId);
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			boolean running = true;
			while (running){
				try {
					sk = receivingSocket.accept();
					MessageHandler mh = new MessageHandler(sk);
					Thread t = new Thread(mh);
					t.start();
				} catch (IOException e) {
					e.getMessage();
				}			
			}		
		}		
	}
	
	/*
	 * This class extract the information from the received object(Message class)
	 */
	class MessageHandler implements Runnable{
		Socket s;
		ObjectInputStream in_obj;
		Message message;
		public MessageHandler(Socket s){
			this.s = s;		
		}
		@Override
		public void run() {
			try {
				in_obj = new ObjectInputStream(s.getInputStream());
				message = (Message)in_obj.readObject();
				if(message.getKind() == 0){       //online node list from the server
					updateOnlineNodes(message);				
				}
				
				if(message.getKind() == 1) {      //a key from another node
					
				}
				
				if(message.getKind() == 2) {    // a lookup request from another node
					
				}
				s.close();
			} catch (IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// TODO Auto-generated method stub
			
		}
		
		
	}
}
