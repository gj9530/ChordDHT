import java.io.Serializable;
import java.util.*;

public class Message implements Serializable {
	private int kind;  //for each kind the object contains 0:onlineNodes, 1:key, 2:nodeInfo
	private String key;
	private HashMap<Integer,String> onlineNodes = new HashMap<>();
	private ArrayList<String> nodeInfo;
	private boolean isDestination;
	private int lookupNode;
	
	public void setKind(int kind){
		this.kind = kind;
	}
	
	public void setKey(String key){
		this.key = key;
	}
	
	public void setOnlineNodes(HashMap<Integer,String> onlineNodes){
		this.onlineNodes = onlineNodes;
	}
	
	public void setNodeInfo(ArrayList<String> nodeInfo){
		this.nodeInfo = nodeInfo;
	}
	
	public void setIsDestination(boolean isDestination){
		this.isDestination = isDestination;
	}
	
	public void setLookupNode(int lookupNode){
		this.lookupNode = lookupNode;
	}
	
	public int getKind(){
		return kind;
	}
	
	public String getKey(){
		return key;
	}
	
	public HashMap<Integer,String> getOnlineNodes(){
		return onlineNodes;
	}
	
	public ArrayList<String> getNodeInfo(){
		return nodeInfo;
	}
	
	public boolean getIsDesination(){
		return isDestination;
	}
	
	public int getLookupNode(){
		return lookupNode;
	}
}
